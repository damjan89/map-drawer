import { Component, Vue, Watch } from 'vue-property-decorator'
import Papa from 'papaparse'
import axios, { AxiosResponse } from 'axios'
// eslint-disable-next-line @typescript-eslint/no-var-requires
@Component({
  components: {}
})
export default class App extends Vue {
  $refs!: {
    canvas: HTMLCanvasElement;
  }

  public clusters: any[]
  public data: any[]
  public labelsData: any[]
  public mouse: any
  public displayTransform: any
  public isDragging = false
  public showBlur = false
  public dragStart = { x: 0, y: 0 }
  public initialPinchDistance = {}
  public cameraOffset = { x: window.innerWidth / 2, y: window.innerHeight / 2 }
  public cameraZoom = 1
  public aspectRatio = 340
  public MAX_ZOOM = 5
  public fontSize = 5
  public blur = 5
  public colorStop = 0.1
  public MIN_ZOOM = 0.1
  public SCROLL_SENSITIVITY = 0.0005
  public lastZoom = this.cameraZoom
  constructor () {
    super()
    this.data = []
    this.labelsData = []
    this.clusters = [
      { id: 1, color: 'rgba(214, 39, 40, 1)' },
      { id: 2, color: 'rgba(44, 160, 44, 1)' },
      { id: 3, color: 'rgba(31, 119, 180, 1)' },
      { id: 4, color: 'rgba(188, 189, 34, 1)' },
      { id: 5, color: 'rgba(148, 103, 189, 1)' },
      { id: 6, color: 'rgba(23, 190, 207, 1)' },
      { id: 7, color: 'rgba(255, 127, 14, 1)' },
      { id: 8, color: 'rgba(140, 86, 75, 1)' },
      { id: 9, color: 'rgba(227, 119, 194, 1)' },
      { id: 10, color: 'rgba(255, 152, 150, 1)' },
      { id: 11, color: 'rgba(152, 223, 138, 1)' },
      { id: 12, color: 'rgba(174, 199, 232, 1)' },
      { id: 13, color: 'rgba(219, 219, 141, 1)' },
      { id: 14, color: 'rgba(197, 176, 213, 1)' },
      { id: 15, color: 'rgba(158, 218, 229, 1)' },
      { id: 16, color: 'rgba(255, 187, 120, 1)' },
      { id: 17, color: 'rgba(196, 156, 148, 1)' },
      { id: 18, color: 'rgba(247, 182, 210, 1)' },
      { id: 19, color: 'rgba(191, 191, 191, 1)' },
      { id: 20, color: 'rgba(191, 191, 191, 1)' },
      { id: 21, color: 'rgba(191, 191, 191, 1)' },
      { id: 22, color: 'rgba(191, 191, 191, 1)' },
      { id: 23, color: 'rgba(191, 191, 191, 1)' },
      { id: 24, color: 'rgba(191, 191, 191, 1)' },
      { id: 25, color: 'rgba(191, 191, 191, 1)' }]
  }

  @Watch('blur', { immediate: false, deep: true })
  public changeBlur (newVal: any) {
    if (newVal) {
      const canvas: any = this.$refs.canvas
      const context = canvas.getContext('2d')
      context.setTransform(1, 0, 0, 1, 0, 0)
      context.clearRect(0, 0, canvas.width, canvas.height)
      context.translate(canvas.width / 2, canvas.height / 2)
      this.blur = newVal
      this.draw()
    }
  }

  @Watch('fontSize', { immediate: false, deep: true })
  public changeFontSize (newVal: any) {
    if (newVal) {
      const canvas: any = this.$refs.canvas
      const context = canvas.getContext('2d')
      context.setTransform(1, 0, 0, 1, 0, 0)
      context.clearRect(0, 0, canvas.width, canvas.height)
      context.translate(canvas.width / 2, canvas.height / 2)
      this.fontSize = newVal
      this.draw()
    }
  }

  @Watch('colorStop', { immediate: false, deep: true })
  public changeColorStop (newVal: any) {
    if (newVal) {
      const canvas: any = this.$refs.canvas
      const context = canvas.getContext('2d')
      context.setTransform(1, 0, 0, 1, 0, 0)
      context.clearRect(0, 0, canvas.width, canvas.height)
      context.translate(canvas.width / 2, canvas.height / 2)
      this.colorStop = newVal
      this.draw()
    }
  }

  @Watch('showBlur', { immediate: false, deep: true })
  public changeShowBlur (newVal: any) {
    const canvas: any = this.$refs.canvas
    const context = canvas.getContext('2d')
    context.setTransform(1, 0, 0, 1, 0, 0)
    context.clearRect(0, 0, canvas.width, canvas.height)
    context.translate(canvas.width / 2, canvas.height / 2)
    this.showBlur = newVal
    this.draw()
  }

  public mounted () {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this
    const canvas: any = this.$refs.canvas
    canvas.addEventListener('mousedown', this.onPointerDown)
    canvas.addEventListener('touchstart', (e: any) => this.handleTouch(e, this.onPointerDown))
    canvas.addEventListener('mouseup', this.onPointerUp)
    canvas.addEventListener('touchend', (e: any) => this.handleTouch(e, this.onPointerUp))
    canvas.addEventListener('mousemove', this.onPointerMove)
    canvas.addEventListener('touchmove', (e: any) => this.handleTouch(e, this.onPointerMove))
    canvas.addEventListener('wheel', (e: any) => this.adjustZoom(e.deltaY * this.SCROLL_SENSITIVITY))
    this.loadFiles()
  }

  public draw () {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this
    const canvas: any = this.$refs.canvas
    const ctx: any = canvas.getContext('2d')
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight

    // Translate to the canvas centre before zooming - so you'll always zoom on what you're looking directly at
    ctx.translate(window.innerWidth / 2, window.innerHeight / 2)
    ctx.scale(this.cameraZoom, this.cameraZoom)
    ctx.translate(-window.innerWidth / 2 + this.cameraOffset.x, -window.innerHeight / 2 + this.cameraOffset.y)
    ctx.clearRect(0, 0, window.innerWidth, window.innerHeight)
    if (!this.data[1]) return
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].x && this.data[i].y) {
        if (this.showBlur) {
          const radius = this.data[i].dotRadius / this.blur
          let color = this.data[i].color
          color = color.replace(', 1)', ', .4)')
          const color0 = color.replace(', .4)', ', 0)')
          const radgrad = ctx.createRadialGradient(this.data[i].x * this.aspectRatio, this.data[i].y * this.aspectRatio, 0, this.data[i].x * this.aspectRatio, this.data[i].y * this.aspectRatio, radius)
          radgrad.addColorStop(0, this.data[i].color)
          radgrad.addColorStop(0.1, color)
          radgrad.addColorStop(this.colorStop, color0)
          // draw shape
          ctx.fillStyle = radgrad
          ctx.fillRect((-canvas.width / 2), (-canvas.height / 2), canvas.width, canvas.height)
        } else {
          const radius = this.data[i].dotRadius / 100
          const color = this.data[i].color
          ctx.fillStyle = color
          ctx.beginPath()
          ctx.arc(this.data[i].x * this.aspectRatio, this.data[i].y * this.aspectRatio, radius, 0, 2 * Math.PI)
          ctx.fill()
        }
      }
    }
    this.drawLabels()

    requestAnimationFrame(this.draw)
  }

  public getEventLocation (e: any) {
    if (e.touches && e.touches.length === 1) {
      return { x: e.touches[0].clientX, y: e.touches[0].clientY }
    } else if (e.clientX && e.clientY) {
      return { x: e.clientX, y: e.clientY }
    }
  }

  public drawRect (x: any, y: any, width: any, height: any) {
    const canvas: any = this.$refs.canvas
    const ctx: any = canvas.getContext('2d')
    ctx.fillRect(x, y, width, height)
  }

  public drawText (text: any, x: any, y: any, size: any, font: any) {
    const canvas: any = this.$refs.canvas
    const ctx: any = canvas.getContext('2d')
    ctx.font = `${size}px ${font}`
    ctx.fillText(text, x, y)
  }

  public onPointerDown (e: any) {
    this.isDragging = true
    if (this.getEventLocation(e)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      this.dragStart.x = this.getEventLocation(e).x / this.cameraZoom - this.cameraOffset.x
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      this.dragStart.y = this.getEventLocation(e).y / this.cameraZoom - this.cameraOffset.y
    }
  }

  public onPointerUp (e: any) {
    this.isDragging = false
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    this.initialPinchDistance = null
    this.lastZoom = this.cameraZoom
  }

  public onPointerMove (e: any) {
    if (this.isDragging) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      this.cameraOffset.x = this.getEventLocation(e).x / this.cameraZoom - this.dragStart.x
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      this.cameraOffset.y = this.getEventLocation(e).y / this.cameraZoom - this.dragStart.y
    }
  }

  public handleTouch (e: any, singleTouchHandler: any) {
    if (e.touches.length === 1) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      this.singleTouchHandler(e)
    } else if (e.type === 'touchmove' && e.touches.length === 2) {
      this.isDragging = false
      this.handlePinch(e)
    }
  }

  public handlePinch (e: any) {
    e.preventDefault()

    const touch1 = { x: e.touches[0].clientX, y: e.touches[0].clientY }
    const touch2 = { x: e.touches[1].clientX, y: e.touches[1].clientY }

    // This is distance squared, but no need for an expensive sqrt as it's only used in ratio
    const currentDistance = (touch1.x - touch2.x) ** 2 + (touch1.y - touch2.y) ** 2

    if (this.initialPinchDistance == null) {
      this.initialPinchDistance = currentDistance
    } else {
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      this.adjustZoom(null, currentDistance / this.initialPinchDistance)
    }
  }

  public adjustZoom (zoomAmount: any, zoomFactor?: any) {
    if (!this.isDragging) {
      if (zoomAmount) {
        this.cameraZoom += zoomAmount
      } else if (zoomFactor) {
        console.log(zoomFactor)
        this.cameraZoom = zoomFactor * this.lastZoom
      }

      this.cameraZoom = Math.min(this.cameraZoom, this.MAX_ZOOM)
      this.cameraZoom = Math.max(this.cameraZoom, this.MIN_ZOOM)

      console.log(zoomAmount)
    }
  }

  public getClusterColor (id: any) {
    const ind: any = this.clusters.findIndex((c: any) => c.id === parseInt(id))
    if (ind > -1) {
      return this.clusters[ind].color
    }
    return 'rgb(40 150 60)'
  }

  public readFile (fileInput: any) {
    /* eslint-disable */
    const self = this
    const reader = new FileReader()
    reader.onload = async function (result: any) {
      self.populateData(result.currentTarget.result)
    }
    reader.readAsBinaryString(fileInput.currentTarget.files[0])
  }
  public async populateData (rawData: any) {
    /* eslint-disable */
    const self = this
    self.data = []
    const finalData: any[] = []
    let data = await Papa.parse(rawData)
    for (let i = 1; i < data.data.length; i++) {
      finalData.push({
        id: data.data[i][0],
        label: data.data[i][1],
        x: data.data[i][2],
        y: data.data[i][3],
        color: self.getClusterColor(data.data[i][4]),
        dotRadius: data.data[i][5]
      })
    }
    self.data = finalData
    self.draw()
  }

  public readLabels (fileInput: any) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this
    const reader = new FileReader()
    reader.onload = async function (result: any) {
      self.labelsData = []
      self.populateLabels(result.currentTarget.result)
    }
    reader.readAsBinaryString(fileInput.currentTarget.files[0])
  }

  public async populateLabels (rawData: any) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this
    const finalData: any[] = []
    const data = await Papa.parse(rawData)
    for (let i = 1; i < data.data.length; i++) {
      if (data.data[i][0] && data.data[i][1] && data.data[i][2] && data.data[i][3]) {
        finalData.push({
          id: data.data[i][0],
          label: data.data[i][1],
          x: data.data[i][2],
          y: data.data[i][3],
          dotRadius: data.data[i][5]
        })
      }
    }
    self.labelsData = finalData
    self.drawLabels()
  }

  public drawLabels () {
    if (this.labelsData.length) {
      const canvas: any = this.$refs.canvas
      const ctx: any = canvas.getContext('2d')
      if (!this.data[1]) return
      for (let i = 0; i < this.labelsData.length; i++) {
        if (this.labelsData[i].x && this.labelsData[i].y) {
          ctx.fillStyle = 'rgba(225, 225, 225, .5)'
          ctx.font = `${this.fontSize}px Arial`
          ctx.fillText(this.labelsData[i].label, this.labelsData[i].x * this.aspectRatio, this.labelsData[i].y * this.aspectRatio)
        }
      }
    }
  }

  public loadFiles () {
    axios.get('http://ov.mk/md/test2.csv?q=' + Math.random()).then((resp: AxiosResponse) => {
      this.populateData(resp.data)
    })
    axios.get('http://ov.mk/md/FirmName.csv?q=' + Math.random()).then((resp: AxiosResponse) => {
      this.populateLabels(resp.data)
    })
  }
}
